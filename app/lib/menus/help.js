'use strict';

const open = require('open');

exports = module.exports = (app) => {
  return {
    label: 'Help',
    submenu: [
      {
        label: 'Online Documentation',
        click: () => open('https://support.office.com/en-us/teams?omkt=en-001')
      },
      {
        label: 'Gitlab Project',
        click: () => open('https://gitlab.com/gilesreece/teams-linux')
      },
      { type: 'separator' },
      {
        label: `Version ${app.getVersion()}`,
        enabled: true
      }
    ]
  };
};